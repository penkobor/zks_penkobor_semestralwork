package penkoborsemwork.ui.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import penkoborsemwork.ui.forms.ReviewForm;

public class Response implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(ReviewForm.ERRORS)
                .viewedBy(actor)
                .asString();
    }

    public static Question<String> displayed() {
        return new Response();
    }
}
