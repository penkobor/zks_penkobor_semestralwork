package penkoborsemwork.ui.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import penkoborsemwork.ui.forms.AddProductToCartForm;

public class SuccessfullyAddedToCart implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(AddProductToCartForm.SUCCESS_SMSNG)
                .viewedBy(actor)
                .asString();
    }

    public static Question<String> displayed() {
        return new SuccessfullyAddedToCart();
    }

}
