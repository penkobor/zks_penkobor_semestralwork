package penkoborsemwork.ui.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import penkoborsemwork.ui.forms.AddProductToCartForm;

public class FailedAddingToCart implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(AddProductToCartForm.FAIL)
                .viewedBy(actor)
                .asString();
    }

    public static Question<String> displayed() {
        return new FailedAddingToCart();
    }

}
