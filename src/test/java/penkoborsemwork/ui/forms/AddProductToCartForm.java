package penkoborsemwork.ui.forms;

import net.serenitybdd.screenplay.targets.Target;

public class AddProductToCartForm {

    public static Target RADIO_SMALL   = Target.the("radio_small").locatedBy("//*[@id=\"input-option218\"]/div[1]/label/input");
    public static Target RADIO_LARGE   = Target.the("radio_large").locatedBy("//*[@id=\"input-option218\"]/div[2]/label/input");
    public static Target CHECKBOX1     = Target.the("checkbox1").locatedBy("//*[@id=\"input-option223\"]/div[1]/label/input");
    public static Target CHECKBOX2     = Target.the("checkbox2").locatedBy("//*[@id=\"input-option223\"]/div[2]/label/input");
    public static Target CHECKBOX3     = Target.the("checkbox3").locatedBy("//*[@id=\"input-option223\"]/div[3]/label/input");
    public static Target CHECKBOX4     = Target.the("checkbox4").locatedBy("//*[@id=\"input-option223\"]/div[4]/label/input");
    public static Target TEXT          = Target.the("text").locatedBy("//*[@id=\"input-option208\"]");
    public static Target SELECT        = Target.the("select").locatedBy("//*[@id=\"input-option217\"]");
    public static Target TEXTAREA      = Target.the("textArea").locatedBy("//*[@id=\"input-option209\"]");
    public static Target FILE_INPUT    = Target.the("fileInput").locatedBy("//*[@id=\"button-upload222\"]");
    public static Target DATE          = Target.the("date").locatedBy("//*[@id=\"input-option219\"]");
    public static Target TIME          = Target.the("time").locatedBy("//*[@id=\"input-option221\"]");
    public static Target DATE_AND_TIME = Target.the("dateAndTime").locatedBy("//*[@id=\"input-option220\"]");
    public static Target QUANTITY      = Target.the("quantity").locatedBy("//*[@id=\"input-quantity\"]");
    public static Target ADD_BUTTON    = Target.the("add").locatedBy("//*[@id=\"button-cart\"]");
    public static Target FAIL = Target.the("success").locatedBy("//*[@id=\"product\"]/div[6]/div");
    public static Target SUCCESS_SMSNG = Target.the("smsngSuccess").locatedBy("//*[@id=\"product-product\"]/div[1]");

}