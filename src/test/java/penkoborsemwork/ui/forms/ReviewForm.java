package penkoborsemwork.ui.forms;

import net.serenitybdd.screenplay.targets.Target;
// rename to CommentSection
public class ReviewForm {

    public static Target REVIEWS = Target.the("review").locatedBy("//*[@id=\"content\"]/div[1]/div[1]/ul[2]/li[3]/a");

    public static Target NAME_FIELD = Target.the("name_field").locatedBy("//*[@id=\"input-name\"]");

    public static Target REVIEW_FIELD = Target.the("review_field").locatedBy("//*[@id=\"input-review\"]");

    public static Target RATING_BUTTON = Target.the("rating_button").locatedBy("//*[@id=\"form-review\"]/div[4]/div/input[3]");

    public static Target CONTINUE_BUTTON = Target.the("continue_button").locatedBy("//*[@id=\"button-review\"]");

    public static Target ERRORS = Target.the("warning").locatedBy("//*[@id=\"form-review\"]/div[2]");

}
