package penkoborsemwork.ui.forms;


import net.serenitybdd.screenplay.targets.Target;

public class LogInElements {
    public static Target MY_ACCOUNT = Target.the("my_account").locatedBy("//*[@id=\"top-links\"]/ul/li[2]/a/span[1]");
    public static Target LOGIN = Target.the("log_in").locatedBy("//*[@id=\"top-links\"]/ul/li[2]/ul/li[2]/a");
    public static Target LOGIN_MAIL = Target.the("email").locatedBy("//*[@id=\"input-email\"]");
    public static Target LOGIN_PASSWORD = Target.the("password").locatedBy("//*[@id=\"input-password\"]");
    public static Target LOGIN_BUTTON = Target.the("login_button").locatedBy("//*[@id=\"content\"]/div/div[2]/div/form/input");
}
