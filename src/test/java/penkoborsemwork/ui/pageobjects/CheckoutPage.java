package penkoborsemwork.ui.pageobjects;

import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import penkoborsemwork.ui.utils.Field;

public class CheckoutPage extends PageObject {

    WebDriver driver;

    String pathToStepOneContinueButton = "//*[@id=\"button-account\"]";
    String pathToStepOneGuestAccount = "//*[@id=\"collapse-checkout-option\"]/div/div/div[1]/div[2]/label/input";
    String pathToStepOneEmail = "//*[@id=\"input-email\"]";
    String pathToStepOnePassword = "//*[@id=\"input-password\"]";
    String pathToStepOneLogIn = "//*[@id=\"button-login\"]";
    String pathToStepTwoContinueButton = "//*[@id=\"button-guest\"]";
    String pathToStepTwoFirstName = "//*[@id=\"input-payment-firstname\"]";
    String pathToStepTwoLastName = "//*[@id=\"input-payment-lastname\"]";
    String pathToStepTwoEmail = "//*[@id=\"input-payment-email\"]";
    String pathToStepTwoTelephone = "//*[@id=\"input-payment-telephone\"]";
    String pathToStepTwoCompany = "//*[@id=\"input-payment-company\"]";
    String pathToStepTwoAddress1 = "//*[@id=\"input-payment-address-1\"]";
    String pathToStepTwoAddress2 = "//*[@id=\"input-payment-address-2\"]";
    String pathToStepTwoCity = "//*[@id=\"input-payment-city\"]";
    String pathToStepTwoPostCode = "//*[@id=\"input-payment-postcode\"]";
    String pathToStepTwoCountry = "//*[@id=\"input-payment-country\"]";
    String pathToStepTwoRegion = "//*[@id=\"input-payment-zone\"]";
    String pathToStepTwoCheckBox = "//*[@id=\"collapse-payment-address\"]/div/div[2]/label/input";
    String pathToStepThreeContinueButton = "//*[@id=\"button-guest-shipping\"]";
    String pathToStepThreeFirstName = "//*[@id=\"input-shipping-firstname\"]";
    String pathToStepThreeLastName = "//*[@id=\"input-shipping-lastname\"]";
    String pathToStepThreeCompany = "//*[@id=\"input-shipping-company\"]";
    String pathToStepThreeAddress1 = "//*[@id=\"input-shipping-address-1\"]";
    String pathToStepThreeAddress2 = "//*[@id=\"input-shipping-address-2\"]";
    String pathToStepThreeCity = "//*[@id=\"input-shipping-city\"]";
    String pathToStepThreePostCode = "//*[@id=\"input-shipping-postcode\"]";
    String pathToStepThreeCountry = "//*[@id=\"input-shipping-country\"]";
    String pathToStepThreeRegion = "//*[@id=\"input-shipping-zone\"]";
    String pathToStepFourContinueButton = "//*[@id=\"button-shipping-method\"]";
    String pathToStepFourFlatRate = "//*[@id=\"collapse-shipping-method\"]/div/div[1]/label/input";
    String pathToStepFourComment = "//*[@id=\"collapse-shipping-method\"]/div/p[4]/textarea";
    String pathToStepFiveContinueButton = "//*[@id=\"button-payment-method\"]";
    String pathToStepFiveCashOnDelivery = "//*[@id=\"collapse-payment-method\"]/div/div[1]/label/input";
    String pathToStepFiveTermsAndConditions = "//*[@id=\"collapse-payment-method\"]/div/div[2]/div/input[1]";
    String pathToStepFiveComments = "//*[@id=\"collapse-payment-method\"]/div/p[3]/textarea";
    String pathToStepSixConfirmOrder = "//*[@id=\"button-confirm\"]";

    public CheckoutPage(WebDriver dr){
        this.driver = dr;
    }

    public void select(Field item){
        switch (item) {
            case stepOneContinueButton: {
                driver.findElement(By.xpath(pathToStepOneContinueButton)).click();
                break;
            }
            case stepOneGuestAccount: {
                driver.findElement(By.xpath(pathToStepOneGuestAccount)).click();
                break;
            }
            case stepOneLogIn: {
                driver.findElement(By.xpath(pathToStepOneLogIn)).click();
                break;
            }
            case stepTwoContinueButton: {
                driver.findElement(By.xpath(pathToStepTwoContinueButton)).click();
                break;
            }
            case stepThreeContinueButton: {
                driver.findElement(By.xpath(pathToStepThreeContinueButton)).click();
                break;
            }
            case stepFourContinueButton: {
                driver.findElement(By.xpath(pathToStepFourContinueButton)).click();
                break;
            }
            case stepFourFlatRate: {
                driver.findElement(By.xpath(pathToStepFourFlatRate)).click();
                break;
            }
            case stepFiveCashOnDelivery: {
                driver.findElement(By.xpath(pathToStepFiveCashOnDelivery)).click();
                break;
            }
            case stepFiveContinueButton: {
                driver.findElement(By.xpath(pathToStepFiveContinueButton)).click();
                break;
            }
            case stepFiveTermsAndConditions: {
                driver.findElement(By.xpath(pathToStepFiveTermsAndConditions)).click();
                break;
            }
            case stepSixConfirmOrder: {
                driver.findElement(By.xpath(pathToStepSixConfirmOrder)).click();
                break;
            }
            case stepTwoCheckBox: {
                driver.findElement(By.xpath(pathToStepTwoCheckBox)).click();
                break;
            }
        }
    }

    public void input(Field item, String input) {
        switch (item) {
            case stepThreeAddress1: {
                driver.findElement(By.xpath(pathToStepThreeAddress1)).sendKeys(input);
                break;
            }
            case stepThreeLastName: {
                driver.findElement(By.xpath(pathToStepThreeLastName)).sendKeys(input);
                break;
            }
            case stepOneLogIn: {
                driver.findElement(By.xpath(pathToStepOneEmail)).sendKeys(input);
                break;
            }
            case stepOnePassword: {
                driver.findElement(By.xpath(pathToStepOnePassword)).sendKeys(input);
            }
            case stepTwoFirstName: {
                driver.findElement(By.xpath(pathToStepTwoFirstName)).sendKeys(input);
                break;
            }
            case stepTwoLastName: {
                driver.findElement(By.xpath(pathToStepTwoLastName)).sendKeys(input);
                break;
            }
            case stepTwoAddress1: {
                driver.findElement(By.xpath(pathToStepTwoAddress1)).sendKeys(input);
                break;
            }
            case stepTwoAddress2: {
                driver.findElement(By.xpath(pathToStepTwoAddress2)).sendKeys(input);
                break;
            }
            case stepTwoCity: {
                driver.findElement(By.xpath(pathToStepTwoCity)).sendKeys(input);
                break;
            }
            case stepTwoCompany: {
                driver.findElement(By.xpath(pathToStepTwoCompany)).sendKeys(input);
                break;
            }
            case stepTwoCountry: {
                Select dropdown = new Select(driver.findElement(By.xpath(pathToStepTwoCountry)));
                dropdown.selectByVisibleText(input);
                //driver.findElement(By.xpath(pathToStepTwoCountry)).sendKeys(input);
                break;
            }
            case stepTwoEmail: {
                driver.findElement(By.xpath(pathToStepTwoEmail)).sendKeys(input);
                break;
            }
            case stepTwoRegion: {
                Select dropdown = new Select(driver.findElement(By.xpath(pathToStepTwoRegion)));
                dropdown.selectByVisibleText(input);
                //driver.findElement(By.xpath(pathToStepTwoRegion)).sendKeys(input);
                break;
            }
            case stepTwoPostCode: {
                driver.findElement(By.xpath(pathToStepTwoPostCode)).sendKeys(input);
                break;
            }
            case stepTwoTelephone: {
                driver.findElement(By.xpath(pathToStepTwoTelephone)).sendKeys(input);
                break;
            }
        }
    }

}
