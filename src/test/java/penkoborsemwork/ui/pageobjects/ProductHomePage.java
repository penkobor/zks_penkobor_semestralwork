package penkoborsemwork.ui.pageobjects;

import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("http://tutorialsninja.com/demo/index.php?route=product/product&path=25_28&product_id=42")
public class ProductHomePage extends PageObject {
}
