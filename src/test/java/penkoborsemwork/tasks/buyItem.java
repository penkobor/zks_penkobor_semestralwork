package penkoborsemwork.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Upload;
import net.serenitybdd.screenplay.actions.selectactions.SelectByIndexFromTarget;
import penkoborsemwork.ui.forms.AddProductToCartForm;

import java.nio.file.Paths;

public class buyItem implements Task {
    private final String text;
    private final String textArea;
    private final String fileInput;
    private final String date;
    private final String time;
    private final String dateAndTime;

    public buyItem(String text, String textArea, String fileInput, String date, String time, String dateAndTime) {
        this.text = text;
        this.textArea = textArea;
        this.fileInput = fileInput;
        this.date = date;
        this.time = time;
        this.dateAndTime = dateAndTime;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(AddProductToCartForm.RADIO_SMALL));
        actor.attemptsTo(Click.on(AddProductToCartForm.CHECKBOX1));
        actor.attemptsTo(Enter.theValue(text).into(AddProductToCartForm.TEXT));
        actor.attemptsTo(new SelectByIndexFromTarget(AddProductToCartForm.SELECT, 1));
        actor.attemptsTo(Enter.theValue(textArea).into(AddProductToCartForm.TEXTAREA));
        actor.attemptsTo(Upload.theFile(Paths.get(fileInput)).to(AddProductToCartForm.FILE_INPUT));
        //actor.attemptsTo(Enter.theValue(fileInput).into(AddProductToCartForm.FILE_INPUT));
        actor.attemptsTo(Enter.theValue(date).into(AddProductToCartForm.DATE));
        actor.attemptsTo(Enter.theValue(time).into(AddProductToCartForm.TIME));
        actor.attemptsTo(Enter.theValue(dateAndTime).into(AddProductToCartForm.DATE_AND_TIME));
        actor.attemptsTo(Click.on(AddProductToCartForm.ADD_BUTTON));
    }

    public static Task withParams(String text, String textArea, String fileInput, String date, String time, String dateAndTime) {
        return Instrumented.instanceOf(buyItem.class)
                .withProperties(text, textArea, fileInput, date, time, dateAndTime);
    }
}
