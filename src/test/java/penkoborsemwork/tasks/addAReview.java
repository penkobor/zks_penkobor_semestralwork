package penkoborsemwork.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import penkoborsemwork.ui.forms.ReviewForm;

public class addAReview implements Task {

    private final String itemName;
    private final String review;
    private final Boolean rating;

    public addAReview(String itemName, String review1, Boolean rating) {
        this.itemName = itemName;
        this.review = review1;
        this.rating = rating;
    }

    @Step("{0} adds an item called '#itemName'")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue(itemName).into(ReviewForm.NAME_FIELD)
        );
        actor.attemptsTo(
                Enter.theValue(review).into(ReviewForm.REVIEW_FIELD)
        );
        if (this.rating) {
            actor.attemptsTo(Click.on(ReviewForm.RATING_BUTTON));
        }
        actor.attemptsTo(Click.on(ReviewForm.CONTINUE_BUTTON));
    }


    public static Task called(String itemName, String review, Boolean rating) {
        return Instrumented.instanceOf(addAReview.class)
                .withProperties(itemName, review, rating);
    }

}