package penkoborsemwork.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import penkoborsemwork.ui.forms.LogInElements;
import penkoborsemwork.ui.forms.ReviewForm;
import penkoborsemwork.ui.pageobjects.ProductHomePage;
import penkoborsemwork.ui.pageobjects.SamsungMonitorPage;


public class StartWith implements Task {

    private final Boolean reviewsOpened;
    private final Boolean loggedIn;
    private final Boolean samsung;

    public StartWith(Boolean reviewsOpened, Boolean loggedIn, Boolean samsung) {
        this.reviewsOpened = reviewsOpened;
        this.loggedIn = loggedIn;
        this.samsung = samsung;
    }

    public static Task productPage(Boolean reviewsOpened, Boolean loggedIn, Boolean samsung) {
        return Instrumented.instanceOf(StartWith.class)
                        .withProperties(reviewsOpened, loggedIn, samsung);

    }

    ProductHomePage productHomePage;
    SamsungMonitorPage samsungMonitorPage;


    @Override
    @Step("{0} starts with an empty todo list")
    public <T extends Actor> void performAs(T actor) {
        if (loggedIn) {
            if (samsung) {
                actor.attemptsTo(Open.browserOn().the(samsungMonitorPage));
            } else {
                actor.attemptsTo(Open.browserOn().the(productHomePage));
            }
            actor.attemptsTo(Click.on(LogInElements.MY_ACCOUNT));
            actor.attemptsTo(Click.on(LogInElements.LOGIN));
            actor.attemptsTo(Enter.theValue("penkoboris95@gmail.com").into(LogInElements.LOGIN_MAIL));
            actor.attemptsTo(Enter.theValue("123456789").into(LogInElements.LOGIN_PASSWORD));
            actor.attemptsTo(Click.on(LogInElements.LOGIN_BUTTON));
        }
        if (samsung) {
            actor.attemptsTo(Open.browserOn().the(samsungMonitorPage));
        } else {
            actor.attemptsTo(Open.browserOn().the(productHomePage));
        }
        if (reviewsOpened){
        actor.attemptsTo(
                Click.on(ReviewForm.REVIEWS)
        );
        }
    }
}
