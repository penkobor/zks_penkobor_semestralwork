package penkoborsemwork.tasks;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import penkoborsemwork.ui.forms.AddProductToCartForm;

public class buySamsungMonitor implements Task {

    @Override
    public <T extends Actor> void performAs(T t) {
        t.attemptsTo(Click.on(AddProductToCartForm.ADD_BUTTON));
    }

    public static Task withParams() {
        return Instrumented.instanceOf(buySamsungMonitor.class).withProperties();
    }
}
