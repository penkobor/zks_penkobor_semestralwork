package penkoborsemwork.features.stories;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import penkoborsemwork.tasks.StartWith;
import penkoborsemwork.tasks.buySamsungMonitor;
import penkoborsemwork.ui.questions.SuccessfullyAddedToCart;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(SerenityRunner.class)
public class AddSamsungToCartStory {
    Actor martin = Actor.named("Martin");

    @Managed(driver = "chrome")
    public WebDriver hisBrowser;

    @Before
    public void justinCanBrowseTheWeb() {
        martin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void shouldSuccessfullyAddItemToCart() {

        givenThat(martin).wasAbleTo(StartWith.productPage(false, false, true));

        when(martin).attemptsTo(buySamsungMonitor.withParams());

        then(martin).should(seeThat(SuccessfullyAddedToCart.displayed(), containsString("Success")));
    }
}