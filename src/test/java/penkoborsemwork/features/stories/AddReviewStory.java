package penkoborsemwork.features.stories;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.junit.annotations.TestData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import penkoborsemwork.tasks.addAReview;
import penkoborsemwork.tasks.StartWith;
import penkoborsemwork.ui.questions.Response;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.*;

@RunWith(SerenityRunner.class)
public class AddReviewStory {
    Actor justin = Actor.named("Justin");

    @Managed(driver = "chrome")
    public WebDriver hisBrowser;

    @Before
    public void justinCanBrowseTheWeb() {
        justin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void shouldGetWarningWhenEnteringInvalidData() throws InterruptedException {

        givenThat(justin).wasAbleTo(StartWith.productPage(true, false, false));

        when(justin).attemptsTo(addAReview.called("title", "review", true));
        Thread.sleep(2000);
        then(justin).should(seeThat(Response.displayed(), containsString("Warning")));

    }

    @Test
    public void shouldSuccessfullyAddCommentAsGuest() throws InterruptedException {

        givenThat(justin).wasAbleTo(StartWith.productPage(true, false, false));

        when(justin).attemptsTo(addAReview.called("title", "LongLongLongReview really really long one", true));
        Thread.sleep(2000);
        then(justin).should(seeThat(Response.displayed(), containsString("Thank")));

    }

    @Test
    public void shouldSuccessfullyAddCommentWhenLoggedIn() throws InterruptedException {

        givenThat(justin).wasAbleTo(StartWith.productPage(true, true, false));

        when(justin).attemptsTo(addAReview.called("Feed the cat", "String that contains more than twenty characters", true));
        Thread.sleep(2000);
        then(justin).should(seeThat(Response.displayed(), containsString("Thank")));
    }
}