package penkoborsemwork.features.stories;


import net.serenitybdd.junit.runners.SerenityRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import penkoborsemwork.ui.pageobjects.CheckoutPage;
import penkoborsemwork.ui.utils.Field;

import java.util.concurrent.TimeUnit;

@RunWith(SerenityRunner.class)
public class CheckoutItemStory {

    public WebDriver driver;

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        this.driver = new ChromeDriver();
    }


    @Test
    public void testIfCheckoutWorksCorrectly() throws InterruptedException {
        prepare(driver);

        CheckoutPage workingPage = new CheckoutPage(driver);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        workingPage.select(Field.stepOneGuestAccount);
        workingPage.select(Field.stepOneContinueButton);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        workingPage.input(Field.stepTwoAddress1, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoAddress2, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCity, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCompany, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCountry, "Ukraine");
        workingPage.input(Field.stepTwoRegion, "Kyiv");
        workingPage.input(Field.stepTwoFirstName, "Angus");
        workingPage.input(Field.stepTwoLastName, "Angus");
        workingPage.input(Field.stepTwoEmail, "angus@fel.cz");
        workingPage.input(Field.stepTwoTelephone, "24234234234");
        workingPage.select(Field.stepTwoContinueButton);
        Thread.sleep(500);

        workingPage.select(Field.stepFourContinueButton);
        Thread.sleep(1000);

        workingPage.select(Field.stepFiveTermsAndConditions);
        Thread.sleep(1000);

        workingPage.select(Field.stepFiveContinueButton);
        Thread.sleep(3000);

        workingPage.select(Field.stepSixConfirmOrder);
        Thread.sleep(2000);
        Assert.assertEquals(driver.getCurrentUrl(), "http://tutorialsninja.com/demo/index.php?route=checkout/success");
    }


    @Test
    public void testIfCheckoutFails() throws InterruptedException {
        prepare(driver);
        CheckoutPage workingPage = new CheckoutPage(driver);
        fillOutFirstStep(workingPage,driver);
        fillOutSecondStep(workingPage);
        workingPage.select(Field.stepTwoContinueButton);
        Thread.sleep(500);
        workingPage.select(Field.stepFourContinueButton);
        Thread.sleep(1000);
        //workingPage.select(CheckoutPageItem.stepFiveTermsAndConditions);
        Thread.sleep(1000);
        workingPage.select(Field.stepFiveContinueButton);
        Thread.sleep(2000);
        Assert.assertNotNull(driver.findElement(By.xpath("//*[@id=\"collapse-payment-method\"]/div/div[1]")));
    }

    @Test
    public void checkIfErrorOccursInStepTwo() throws InterruptedException {
        prepare(driver);

        CheckoutPage workingPage = new CheckoutPage(driver);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        workingPage.select(Field.stepOneGuestAccount);
        workingPage.select(Field.stepOneContinueButton);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        workingPage.input(Field.stepTwoAddress1, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoAddress2, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCity, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCompany, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCountry, "Ukraine");
        workingPage.input(Field.stepTwoRegion, "Kyiv");

        workingPage.input(Field.stepTwoLastName, "Angus");
        workingPage.input(Field.stepTwoEmail, "angus@fel.cz");
        workingPage.input(Field.stepTwoTelephone, "24234234234");
        workingPage.select(Field.stepTwoContinueButton);
        Thread.sleep(1000);

        Assert.assertNotNull(driver.findElement(By.xpath("//*[@id=\"account\"]/div[2]/div")));
    }

    @Test
    public void checkIfErrorOccursInStepThree() throws InterruptedException {
        prepare(driver);
        CheckoutPage workingPage = new CheckoutPage(driver);
        fillOutFirstStep(workingPage,driver);
        fillOutSecondStep(workingPage);
        workingPage.select(Field.stepTwoCheckBox);
        workingPage.select(Field.stepTwoContinueButton);
        Thread.sleep(1000);
        // third step
        workingPage.input(Field.stepThreeAddress1,"a");
        workingPage.input(Field.stepThreeLastName, "2");
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"button-guest-shipping\"]")).click();
        Thread.sleep(1000);
        Assert.assertNotNull(driver.findElement(By.xpath("//*[@id=\"collapse-shipping-address\"]/div/form/div[1]/div/div")));

    }

    @Test
    public void testCheckoutWithLogin() throws InterruptedException {
        prepare(driver);
        CheckoutPage workingPage = new CheckoutPage(driver);
        Thread.sleep(1000);
        workingPage.input(Field.stepOneLogIn, "penkoboris95@gmail.com");
        driver.findElement(By.xpath("//*[@id=\"input-password\"]")).sendKeys("123456789");
        workingPage.select(Field.stepOneLogIn);

        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"button-payment-address\"]")).click();
        Thread.sleep(1000);

        driver.findElement(By.xpath("//*[@id=\"button-shipping-address\"]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"button-shipping-method\"]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"collapse-payment-method\"]/div/div[2]/div/input[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"button-payment-method\"]")).click();

        Thread.sleep(3000);
        driver.findElement(By.xpath("//*[@id=\"button-confirm\"]")).click();
        Thread.sleep(2000);
        Assert.assertEquals(driver.getCurrentUrl(), "http://tutorialsninja.com/demo/index.php?route=checkout/success");
    }

    @After
    public void tearDown(){
       driver.quit();
    }


    private void fillOutFirstStep(CheckoutPage workingPage, WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        workingPage.select(Field.stepOneGuestAccount);
        workingPage.select(Field.stepOneContinueButton);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    private void fillOutSecondStep(CheckoutPage workingPage) {
        workingPage.input(Field.stepTwoAddress1, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoAddress2, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCity, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCompany, "sdfsdfsdfsdfsdfsdfsdfsdfds");
        workingPage.input(Field.stepTwoCountry, "Ukraine");
        workingPage.input(Field.stepTwoRegion, "Kyiv");
        workingPage.input(Field.stepTwoFirstName, "Angus");
        workingPage.input(Field.stepTwoLastName, "Angus");
        workingPage.input(Field.stepTwoEmail, "angus@fel.cz");
        workingPage.input(Field.stepTwoTelephone, "24234234234");
    }

    private void prepare(WebDriver driver) {
        driver.get("http://tutorialsninja.com/demo/index.php?route=product/category&path=33");
        driver.findElement(By.xpath("//*[@id=\"content\"]/div[2]/div[2]/div/div[2]/div[2]/button[1]")).click();
        driver.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[5]/a")).click();
    }
}
