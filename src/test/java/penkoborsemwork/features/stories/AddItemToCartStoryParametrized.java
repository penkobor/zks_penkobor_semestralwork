package penkoborsemwork.features.stories;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import net.thucydides.junit.annotations.TestData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import penkoborsemwork.tasks.StartWith;
import penkoborsemwork.tasks.buyItem;
import penkoborsemwork.ui.questions.FailedAddingToCart;


import java.util.Arrays;
import java.util.Collection;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(SerenityParameterizedRunner.class)
//@UseTestDataFrom(value="data.csv")
public class AddItemToCartStoryParametrized {
    Actor martin = Actor.named("Justin");

    private final String text;
    private final String textArea;
    private final String fileInput;
    private final String date;
    private final String time;
    private final String dateAndTime;

    @Managed(driver = "chrome")
    public WebDriver hisBrowser;

    @TestData
    public static Collection<Object[]> testData(){
        return Arrays.asList(new Object[][]{
                {"1", "11", "input.txt", "2011-02-20", "22:25", "2011-02-20 22:25"},
                {"", "11", "input.txt", "2011-02-20", "22:25", "2011-02-20 22:25"},
                {"1", "", "input.txt", "2011-02-20", "22:25", "2011-02-20 22:25"},
                {"1", "11", "", "2011-02-20", "22:25", "2011-02-20 22:25"},
                {"1", "11", "input.txt", "", "22:25", "2011-02-20 22:25"},
                {"1", "11", "input.txt", "2011-02-20", "", "2011-02-20 22:25"},
                {"1", "11", "input.txt", "2011-02-20", "22:25", ""}
        });
    }

    public AddItemToCartStoryParametrized(String text, String textArea, String fileInput, String date, String time, String dateAndTime) {
        this.text = text;
        this.textArea = textArea;
        this.fileInput = fileInput;
        this.date = date;
        this.time = time;
        this.dateAndTime = dateAndTime;
    }

    @Before
    public void justinCanBrowseTheWeb() {
        martin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void shouldGetWarningWhenEnteringInvalidData() {

        givenThat(martin).wasAbleTo(StartWith.productPage(false, false, false));

        when(martin).attemptsTo(buyItem.withParams(text, textArea, fileInput, date, time, dateAndTime));

        then(martin).should(seeThat(FailedAddingToCart.displayed(), containsString("File")));

    }


}
